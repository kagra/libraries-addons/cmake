find_package(Git)

if(GIT_FOUND)

    # Name of the default branch
    set(DEFAULT_GIT_BRANCH "main")

    # Function to get current git branch
    function(get_git_branch GIT_BRANCH)

        # Check if it's a Git repository
        if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.git" AND NOT IS_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/.git")

            file(READ "${CMAKE_CURRENT_SOURCE_DIR}/.git" GIT_FILE_CONTENT)
            string(STRIP "${GIT_FILE_CONTENT}" GIT_FILE_CONTENT_STRIPPED) # Remove leading/trailing whitespace            
            string(SUBSTRING "${GIT_FILE_CONTENT_STRIPPED}" 0 6 FIRST_SIX_CHARS) # Get the first 6 characters
            
            if (NOT FIRST_SIX_CHARS STREQUAL "gitdir")
                message(WARNING "This directory `${CMAKE_CURRENT_SOURCE_DIR}` is not a git submodule.")
            endif()
            return()

        elseif(NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.git/config")
            message(WARNING "This directory `${CMAKE_CURRENT_SOURCE_DIR}` is not a git repository.")
            return()
        endif()

        execute_process(
            COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref --short HEAD
            ERROR_QUIET
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            OUTPUT_VARIABLE _GIT_BRANCH
            OUTPUT_STRIP_TRAILING_WHITESPACE
        )

        string(REPLACE "HEAD" "${DEFAULT_GIT_BRANCH}" _GIT_BRANCH "${_GIT_BRANCH}")
        set(${GIT_BRANCH} ${_GIT_BRANCH} PARENT_SCOPE)  # Set the result to the current git branch

    endfunction()

    # Check if the stored git branch is available
    if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.git" AND NOT IS_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/.git")

        file(READ "${CMAKE_CURRENT_SOURCE_DIR}/.git" GIT_FILE_CONTENT)
        string(STRIP "${GIT_FILE_CONTENT}" GIT_FILE_CONTENT_STRIPPED) # Remove leading/trailing whitespace            
        string(SUBSTRING "${GIT_FILE_CONTENT_STRIPPED}" 0 6 FIRST_SIX_CHARS) # Get the first 6 characters
        
        if (NOT FIRST_SIX_CHARS STREQUAL "gitdir")
            message(WARNING "This directory `${CMAKE_CURRENT_SOURCE_DIR}` is not a git submodule.")
        endif()
        return()

    elseif(NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.git/config")
        message(WARNING "This directory `${CMAKE_CURRENT_SOURCE_DIR}` is not a git repository.")
    else()
        get_git_branch(CURRENT_GIT_BRANCH)
        message(STATUS "Git branch is currently `${CURRENT_GIT_BRANCH}`.")

        if(DEFINED PREVIOUS_GIT_BRANCH)    
            # Compare the stored git branch with the current git branch
            if(NOT "${PREVIOUS_GIT_BRANCH}" STREQUAL "${CURRENT_GIT_BRANCH}")
                message(WARNING "Git branch has changed since the last CMake run: ${PREVIOUS_GIT_BRANCH}")
            endif()
            
            # Store the current git branch for the next CMake run
            set(PREVIOUS_GIT_BRANCH "${CURRENT_GIT_BRANCH}" CACHE STRING "Previous git branch" FORCE)
        endif()
    endif()

endif()