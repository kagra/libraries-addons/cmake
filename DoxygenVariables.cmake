# Set PROJECT_OS to the current system name (e.g., Linux, Windows)
set(PROJECT_OS "${CMAKE_SYSTEM_NAME}")
set(PROJECT_TITLE "${LIBRARY_NAME}")

# Set PROJECT_URL based on environment variables or Git repository URL
if(NOT DEFINED PROJECT_URL)
    set(PROJECT_URL $ENV{PROJECT_URL})
endif()
if(NOT PROJECT_URL)
    set(PROJECT_URL $ENV{CI_PROJECT_URL})
endif()

if(NOT PROJECT_URL)
    # Extract the Git repository URL and format it as an HTTPS URL
    execute_process(
        COMMAND git config --get remote.origin.url
        COMMAND sed -E "s,^([^:/]+)://([^@,]+@)?([^:/]+)(:[0-9]+)?/(.*),https://\\3/\\5,"
        OUTPUT_VARIABLE PROJECT_URL
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
endif()

# Set PROJECT_NUMBER based on environment variables or Git commit information
if(NOT DEFINED PROJECT_NUMBER)
    set(PROJECT_NUMBER $ENV{PROJECT_NUMBER})
endif()
if(NOT PROJECT_NUMBER)
    set(PROJECT_NUMBER $ENV{CI_COMMIT_TAG})
endif()
if(NOT PROJECT_NUMBER)
    # Obtain the commit hash or tag from Git repository
    execute_process(
        OUTPUT_VARIABLE PROJECT_NUMBER_RAW
        ERROR_FILE /dev/null
        COMMAND git rev-parse HEAD 
        COMMAND git describe --tags)
    if(PROJECT_NUMBER_RAW)
        string(REGEX REPLACE "\n$" "" PROJECT_NUMBER ${PROJECT_NUMBER_RAW})
    endif()
endif()
if(NOT PROJECT_NUMBER)
    execute_process(
        COMMAND git rev-parse --short HEAD
        OUTPUT_VARIABLE PROJECT_NUMBER_RAW
        ERROR_QUIET
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    if(PROJECT_NUMBER_RAW)
        string(REGEX REPLACE "\n$" "" PROJECT_NUMBER ${PROJECT_NUMBER_RAW})
    endif()
endif()
if(NOT PROJECT_NUMBER)
    set(PROJECT_NUMBER "unknown")
endif()

# Set PROJECT_TITLE, PROJECT_BRIEF, PROJECT_FOOTER, PROJECT_SYMLINK based on environment variables
if(NOT DEFINED PROJECT_TITLE)
    set(PROJECT_TITLE $ENV{PROJECT_TITLE})
endif()

if(NOT DEFINED PROJECT_BRIEF)
    set(PROJECT_BRIEF $ENV{PROJECT_BRIEF})
endif()

if(NOT DEFINED PROJECT_LOGO)
    set(PROJECT_LOGO $ENV{PROJECT_LOGO})
endif()
if(NOT PROJECT_LOGO)
    set(PROJECT_LOGO "${CMAKE_CURRENT_SOURCE_DIR}/images/logo.drawio.svg") # Assuming this is a typo and meant to be $ENV{PROJECT_LOGO}
endif()

set(IMAGE_PATH "")
if(IS_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/images")
   set(IMAGE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/images ${IMAGE_PATH}")
endif()
if(IS_DIRECTORY "${CMAKE_SOURCE_DIR}/images")
   set(IMAGE_PATH "${CMAKE_SOURCE_DIR}/images ${IMAGE_PATH}")
endif()
if(IS_DIRECTORY "${CMAKE_SOURCE_DIR}/share/images")
   set(IMAGE_PATH "${CMAKE_SOURCE_DIR}/share/images ${IMAGE_PATH}")
endif()

if(NOT DEFINED PROJECT_FOOTER)
    set(PROJECT_FOOTER $ENV{PROJECT_FOOTER})
endif()

if(NOT DEFINED PROJECT_SYMLINK)
    set(PROJECT_SYMLINK $ENV{PROJECT_SYMLINK})
endif()
if(NOT PROJECT_SYMLINK)
    set(PROJECT_SYMLINK ON)
endif()
if(NOT DOXYGEN_MAINPAGE)
    set(DOXYGEN_MAINPAGE "${CMAKE_SOURCE_DIR}/README.md")
endif()
#
# Look for input variables 
# Existing definition of DOXYGEN_INPUT (if any)
set(DOXYGEN_INPUT
    ${DOXYGEN_INPUT}
    ${DOXYGEN_MAINPAGE}
    "${CMAKE_SOURCE_DIR}/README.md"
    "${CMAKE_SOURCE_DIR}/LICENSE"
    "${CMAKE_SOURCE_DIR}/CONTRIBUTING.md"
    "${CMAKE_SOURCE_DIR}/docs"
    "${CMAKE_SOURCE_DIR}/include"
    "${CMAKE_SOURCE_DIR}/examples"
    "${CMAKE_SOURCE_DIR}/src"
    "${CMAKE_SOURCE_DIR}/tests"
)

#
# Check existence of each file/directory in DOXYGEN_INPUT
foreach(INPUT_PATH ${DOXYGEN_INPUT})
    if(NOT EXISTS ${INPUT_PATH})
        list(REMOVE_ITEM DOXYGEN_INPUT ${INPUT_PATH})
    endif()
endforeach()

set(DOXYGEN_FILES)
foreach(INPUT_PATH ${DOXYGEN_INPUT})

    if(IS_DIRECTORY "${INPUT_PATH}")

        file(GLOB_RECURSE _FILES "${INPUT_PATH}/*")
        list(FILTER _FILES EXCLUDE REGEX ".git*")
        list(FILTER _FILES EXCLUDE REGEX "CMakeLists.txt")
        list(FILTER _FILES EXCLUDE REGEX ".*LinkDef.h")
        list(FILTER _FILES EXCLUDE REGEX ".*Config.h")
        list(FILTER _FILES EXCLUDE REGEX ".*.in")

        foreach(_FILE ${_FILES})
            list(APPEND DOXYGEN_FILES ${_FILE})
        endforeach()

    else()

        list(APPEND DOXYGEN_FILES ${INPUT_PATH})

    endif()

endforeach()

string(REPLACE ";" " " DOXYGEN_INPUT "${DOXYGEN_INPUT}")
