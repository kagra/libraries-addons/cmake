
if("${CMAKE_CURRENT_SOURCE_DIR}" STREQUAL "${CMAKE_SOURCE_DIR}" AND EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/../CMakeLists.txt")

    message(FATAL_ERROR "Parent CMakeLists.txt detected. Only execution from parent directory is permitted. ")

endif()
