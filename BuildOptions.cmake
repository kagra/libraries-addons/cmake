set(CMAKE_MAINPROJECT OFF)
if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_SOURCE_DIR}")
   set(CMAKE_MAINPROJECT ON)
endif()

function(update_build_options)

    if(DEFINED BUILD)

        option(BUILD_IMAGE "Build Docker Container" ${BUILD} AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/Dockerfile)
        option(BUILD_LIBRARY "Build library source code" ${BUILD} AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/src)
        option(BUILD_DOCUMENTATION "Build Documentation (and its assets)" ${BUILD} AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/docs)
        option(BUILD_TESTS "Build Tests" ${BUILD} AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tests)
        option(BUILD_EXAMPLES "Build Examples" ${BUILD} AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/examples)
        option(BUILD_TOOLS "Build Tools" ${BUILD} AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tools)
        option(BUILD_PYPROJECT "Build pyproject generator" ${BUILD} AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/pyproject.py)

    else()

        # Set the build type if it's not already defined in cache
        if(NOT DEFINED CMAKE_BUILD_TYPE)
            set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Build type")
        endif()

        # Store the build type in a cache variable for future reference
        if(NOT DEFINED CMAKE_BUILD_TYPE_LAST)
            set(CMAKE_BUILD_TYPE_LAST ${CMAKE_BUILD_TYPE} CACHE STRING "Last known build type")
        endif()

        set(CMAKE_BUILD_TYPE_CHANGED OFF)
        if(NOT "${CMAKE_BUILD_TYPE}" STREQUAL "${CMAKE_BUILD_TYPE_LAST}")
            set(CMAKE_BUILD_TYPE_CHANGED ON)
            message(STATUS "Build type has changed from `${CMAKE_BUILD_TYPE_LAST}` to `${CMAKE_BUILD_TYPE}`")
        endif()

        set(CMAKE_BUILD_TYPE_DEFAULT ON)
        if (NOT DEFINED CMAKE_BUILD_TYPE OR NOT CMAKE_BUILD_TYPE OR CMAKE_BUILD_TYPE STREQUAL "Release")
            set(CMAKE_BUILD_TYPE_DEFAULT OFF)
        endif()

        if(NOT DEFINED BUILD_IMAGE OR CMAKE_BUILD_TYPE_CHANGED)
            set(BUILD_IMAGE ${CMAKE_MAINPROJECT} CACHE BOOL "Build Docker Container" FORCE)
        endif()

        if(NOT DEFINED BUILD_LIBRARY)
            set(BUILD_LIBRARY ON CACHE BOOL "Build library source code" FORCE)
        endif()

        if(NOT DEFINED BUILD_DOCUMENTATION OR CMAKE_BUILD_TYPE_CHANGED)
            set(BUILD_DOCUMENTATION ${CMAKE_MAINPROJECT} CACHE BOOL "Build Documentation" FORCE)
        endif()

        if(NOT DEFINED BUILD_TOOLS OR CMAKE_BUILD_TYPE_CHANGED)
            set(BUILD_TOOLS ${CMAKE_MAINPROJECT} CACHE BOOL "Build Tools" FORCE)
        endif()

        if(NOT DEFINED BUILD_EXAMPLES OR CMAKE_BUILD_TYPE_CHANGED)
            set(BUILD_EXAMPLES ${CMAKE_BUILD_TYPE_DEFAULT} CACHE BOOL "Build Examples" FORCE)
        endif()

        if(NOT DEFINED BUILD_TESTS OR CMAKE_BUILD_TYPE_CHANGED)
            set(BUILD_TESTS ${CMAKE_BUILD_TYPE_DEFAULT} CACHE BOOL "Build Tests" FORCE)
        endif()

        if(NOT DEFINED BUILD_PYPROJECT OR CMAKE_BUILD_TYPE_CHANGED)
            set(BUILD_PYPROJECT ${CMAKE_MAINPROJECT} CACHE BOOL "Build Pyproject" FORCE)
        endif()
    endif()

    if(CMAKE_BUILD_TYPE_CHANGED)
        set(CMAKE_BUILD_TYPE_LAST ${CMAKE_BUILD_TYPE} CACHE STRING "Last known build type" FORCE)
    endif()

endfunction()

function(check_build_option option_name msg)

    set(_MSGTYPE STATUS)
    if(DEFINED BUILD)
        set(_MSGTYPE WARNING)
    endif()

    if(NOT ${option_name})
        message(${_MSGTYPE} "${msg} (use -D${option_name}=ON to enable)")
    endif()
endfunction()
