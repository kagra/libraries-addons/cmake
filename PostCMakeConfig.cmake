# Prepare available commands
set(COMMAND_LIST "make" "make install")

if(BUILD_LIBRARY)
    list(APPEND COMMAND_LIST "make lib")
endif()

if(BUILD_IMAGE AND EXISTS ${CMAKE_BINARY_DIR}/Dockerfile)
    list(APPEND COMMAND_LIST "make image")
    list(APPEND COMMAND_LIST "make image-publish")
endif()

if(BUILD_TOOLS)
    list(APPEND COMMAND_LIST "make tools")
endif()

if(BUILD_DOCUMENTATION)
    list(APPEND COMMAND_LIST "make doxygen")  # Build doxygen documentation
    list(APPEND COMMAND_LIST "make pages") # Build pages
    list(APPEND COMMAND_LIST "make serve") # Serve pages into a ./public ready-to-use
endif()

if(BUILD_EXAMPLES)
    list(APPEND COMMAND_LIST "make examples")
endif()

if(BUILD_TESTS)
    list(APPEND COMMAND_LIST "make tests")
endif()

# Print status
set(COMMANDS "")
foreach( COMMAND ${COMMAND_LIST})
    if("${COMMAND}" STREQUAL "")
        set(COMMANDS "`${COMMAND}`")
    else()
        set(COMMANDS "${COMMANDS}, `${COMMAND}`")
    endif()
endforeach()

message(STATUS "Library compilation configured. It will be compiled in `${CMAKE_BINARY_DIR}`.")
message(STATUS "Installation directory is currently `${CMAKE_INSTALL_PREFIX}`")
message(STATUS "Available commands are: ${COMMANDS}")
