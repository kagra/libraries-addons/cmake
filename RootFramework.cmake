#
# ROOT
list(APPEND CMAKE_MODULE_PATH $ENV{ROOTSYS}/etc/cmake)

# You can call find_package in the main CMakeLists.txt and add some additional components
# (e.g. TreePlayer Geom Spectrum)
if(NOT DEFINED ROOT_FOUND)
    find_package(ROOT REQUIRED)
endif()

include(${ROOT_USE_FILE})
include_directories(${ROOT_INCLUDE_DIRS})

if (ROOT_VERSION VERSION_LESS 6.15/99 OR NOT ROOT_VERSION) # Actually it should be at least 6.18..
        message( FATAL_ERROR "ROOT has to be at least 6.16/00" )
endif()

# Execute root-config --features and capture the output
execute_process(
    COMMAND root-config --features
    OUTPUT_VARIABLE ROOT_FEATURES
    RESULT_VARIABLE RESULT
    ERROR_QUIET
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

if(NOT DEFINED ROOT_RUNTIME_CXXMODULES OR NOT ROOT_RUNTIME_CXXMODULES)
    set(ROOT_RUNTIME_CXXMODULES ON CACHE BOOL "Enable or disable runtime C++ modules")
    message(STATUS "ROOT Runtime C++ modules: ${ROOT_RUNTIME_CXXMODULES}")
else()
    if(NOT "${ROOT_RUNTIME_CXXMODULES}" STREQUAL "$CACHE{ROOT_RUNTIME_CXXMODULES}")
        message(STATUS "ROOT Runtime C++ modules: ${ROOT_RUNTIME_CXXMODULES}")
        set(ROOT_RUNTIME_CXXMODULES ${ROOT_RUNTIME_CXXMODULES} CACHE BOOL "Enable or disable runtime C++ modules" FORCE)
    endif()
endif()

set(runtime_cxxmodules ${ROOT_RUNTIME_CXXMODULES})

function(ROOT_ADD_LIBRARY)

    if(NOT ARGV0)
        message(FATAL_ERROR "LIBRARY argument is required")
    endif()

    set(ROOT_ADD_LIBRARY_LIBRARY ${ARGV0})
    list(REMOVE_AT ARGV 0)

    # Parse arguments
    set(option "VISIBILITY")
    set(oneValueArgs "DICTIONARY" "LINKDEF")
    set(multiValueArgs "SOURCES" "HEADERS" "PACKAGES")
    cmake_parse_arguments(ROOT_ADD_LIBRARY "" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    # Split packages per visibility option
    set(ROOT_ADD_LIBRARY_PACKAGES_VISIBILITY "ROOT_ADD_LIBRARY_PUBLIC_PACKAGES")
    set(ROOT_ADD_LIBRARY_PUBLIC_PACKAGES)
    set(ROOT_ADD_LIBRARY_PRIVATE_PACKAGES)
    set(ROOT_ADD_LIBRARY_INTERFACE_PACKAGES)

    foreach(PKG ${ROOT_ADD_LIBRARY_PACKAGES})

        if(${PKG} STREQUAL "PRIVATE")
            set(ROOT_ADD_LIBRARY_PACKAGES_VISIBILITY "ROOT_ADD_LIBRARY_PRIVATE_PACKAGES")
        elseif(${PKG} STREQUAL "INTERFACE")
            set(ROOT_ADD_LIBRARY_PACKAGES_VISIBILITY "ROOT_ADD_LIBRARY_INTERFACE_PACKAGES")
        elseif(${PKG} STREQUAL "PUBLIC")
            set(ROOT_ADD_LIBRARY_PACKAGES_VISIBILITY "ROOT_ADD_LIBRARY_PUBLIC_PACKAGES")
        else()
            list(APPEND ${ROOT_ADD_LIBRARY_PACKAGES_VISIBILITY} ${PKG})
        endif()

    endforeach()

    if (NOT ROOT_ADD_LIBRARY_VISIBILITY)
        set(ROOT_ADD_LIBRARY_VISIBILITY "PUBLIC")
    endif()

    # Add the library
    add_library(${ROOT_ADD_LIBRARY_LIBRARY} SHARED "${ROOT_ADD_LIBRARY_SOURCES}")
    target_include_directories(${LIBRARY} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
    target_include_directories(${LIBRARY} PUBLIC "${CMAKE_CURRENT_BINARY_DIR}/include")

    # Link libraries and packages
    if (ROOT_ADD_LIBRARY_VISIBILITY STREQUAL "PRIVATE")
        target_link_libraries(${ROOT_ADD_LIBRARY_LIBRARY} PRIVATE ${ROOT_LIBRARIES})
    elseif (ROOT_ADD_LIBRARY_VISIBILITY STREQUAL "INTERFACE")
        target_link_libraries(${ROOT_ADD_LIBRARY_LIBRARY} INTERFACE ${ROOT_LIBRARIES})
    elseif (ROOT_ADD_LIBRARY_VISIBILITY STREQUAL "PUBLIC")
        target_link_libraries(${ROOT_ADD_LIBRARY_LIBRARY} PUBLIC ${ROOT_LIBRARIES})
    else()
        message(FATAL_ERROR "Visibility option not recognized: it must be PUBLIC, PRIVATE or INTERFACE")
    endif()

    foreach(DEPENDENCY ${ROOT_ADD_LIBRARY_PUBLIC_PACKAGES} ${ROOT_ADD_LIBRARY_PRIVATE_PACKAGES} ${ROOT_ADD_LIBRARY_INTERFACE_PACKAGES})

        if(DEFINED ${DEPENDENCY}_FOUND)
            set(ALREADY_FOUND TRUE)
        else()
            set(ALREADY_FOUND FALSE)
        endif()

        if(${DEPENDENCY} IN_LIST ROOT_ADD_LIBRARY_PRIVATE_PACKAGES)
            target_link_package(${ROOT_ADD_LIBRARY_LIBRARY} PRIVATE ${DEPENDENCY})
        elseif(${DEPENDENCY} IN_LIST ROOT_ADD_LIBRARY_INTERFACE_PACKAGES)
            target_link_package(${ROOT_ADD_LIBRARY_LIBRARY} INTERFACE ${DEPENDENCY})
        else()
            target_link_package(${ROOT_ADD_LIBRARY_LIBRARY} PUBLIC ${DEPENDENCY})
        endif()

        if(NOT ALREADY_FOUND AND NOT ${DEPENDENCY}_FOUND)
            message(FATAL_ERROR "Package `${DEPENDENCY}` not found. Use find_package(${DEPENDENCY}) before ROOT_ADD_LIBRARY to make it optional.")
        endif()
    endforeach()

    # Default to shared library if not specified
    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/include/lib${LIBRARY}.Config.h.in)
        CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/include/lib${LIBRARY}.Config.h.in ${CMAKE_CURRENT_BINARY_DIR}/include/lib${LIBRARY}.Config.h @ONLY)
    endif()
    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake/thislib.sh.in)
        CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/thislib.sh.in   thislib.${LIBRARY}.sh   @ONLY )
    endif()
    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake/thislib.csh.in)
        CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/thislib.csh.in  thislib.${LIBRARY}.csh  @ONLY )
    endif()
    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake/rootlogon.in)
        if(APPLE)
            CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/rootlogon.in    .rootlogon   @ONLY )
        else()
            CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/rootlogon.in    rootlogon.C   @ONLY )
        endif()
    endif()

    if(NOT ROOT_ADD_LIBRARY_LINKDEF OR ROOT_ADD_LIBRARY_LINKDEF STREQUAL "")
        set(ROOT_ADD_LIBRARY_LINKDEF include/lib${ROOT_ADD_LIBRARY_LIBRARY}.LinkDef.h)
        if(NOT ROOT_ADD_LIBRARY_LINKDEF_MESSAGE_SHOWN)
            message(STATUS "LinkDef default location used: ${ROOT_ADD_LIBRARY_LINKDEF}")
            set(ROOT_ADD_LIBRARY_LINKDEF_MESSAGE_SHOWN TRUE CACHE INTERNAL "LinkDef message shown")
        endif()
    endif()

    if(NOT ROOT_ADD_LIBRARY_DICTIONARY OR ROOT_ADD_LIBRARY_DICTIONARY STREQUAL "")
        set(ROOT_ADD_LIBRARY_DICTIONARY ${ROOT_ADD_LIBRARY_LIBRARY}.Dict)
        if(NOT ROOT_ADD_LIBRARY_DICTIONARY_MESSAGE_SHOWN)
            message(STATUS "Dictionary default target used: ${ROOT_ADD_LIBRARY_DICTIONARY}")
            set(ROOT_ADD_LIBRARY_DICTIONARY_MESSAGE_SHOWN TRUE CACHE INTERNAL "Dictionary message shown")
        endif()
    endif()

    if(ROOT_RUNTIME_CXXMODULES)
 
        set(_HEADERS)
        foreach(HEADER ${ROOT_ADD_LIBRARY_HEADERS})
            if(NOT IS_ABSOLUTE ${HEADER})
                set(HEADER "${CMAKE_CURRENT_SOURCE_DIR}/${HEADER}")
            endif()
            file(RELATIVE_PATH HEADER ${CMAKE_CURRENT_SOURCE_DIR}/include ${HEADER})
            list(APPEND _HEADERS "../include/${HEADER}")

        endforeach()

        set(ROOT_ADD_LIBRARY_HEADERS ${_HEADERS})
    endif()

    if(NOT IS_ABSOLUTE ${ROOT_ADD_LIBRARY_LINKDEF})
        set(ROOT_ADD_LIBRARY_LINKDEF "${CMAKE_CURRENT_SOURCE_DIR}/${ROOT_ADD_LIBRARY_LINKDEF}")
        file(RELATIVE_PATH ROOT_ADD_LIBRARY_LINKDEF ${CMAKE_CURRENT_SOURCE_DIR}/include ${ROOT_ADD_LIBRARY_LINKDEF})
    endif()

    set(ROOT_ADD_LIBRARY_LINKDEF "../include/${ROOT_ADD_LIBRARY_LINKDEF}")
    list(REMOVE_ITEM ROOT_ADD_LIBRARY_HEADERS ${ROOT_ADD_LIBRARY_LINKDEF})

    execute_process(COMMAND ln -snf ${CMAKE_CURRENT_SOURCE_DIR}/include ..)
    ROOT_GENERATE_DICTIONARY(${ROOT_ADD_LIBRARY_DICTIONARY}
        ${ROOT_ADD_LIBRARY_HEADERS} 
        MODULE ${ROOT_ADD_LIBRARY_LIBRARY} 
        LINKDEF ${CMAKE_CURRENT_SOURCE_DIR}/include/${ROOT_ADD_LIBRARY_LINKDEF}
    )

endfunction()

# Installation procedure
macro(ROOT_INSTALL_LIBRARY LIBRARY)

    if (NOT TARGET ${LIBRARY})
        message(FATAL_ERROR "Target library ${LIBRARY} not found. (Did you forget to call ROOT_ADD_LIBRARY?)")
    endif()

    install(TARGETS ${LIBRARY}
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        RUNTIME DESTINATION bin
    )

    if(NOT ROOT_RUNTIME_CXXMODULES)
        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBRARY}_rdict.pcm DESTINATION lib)
        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBRARY}.rootmap DESTINATION lib)
    else()

        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${LIBRARY}.pcm DESTINATION lib)

        if (EXISTS ${CMAKE_CURRENT_BINARY_DIR}/module.modulemap)
            file(READ ${CMAKE_CURRENT_BINARY_DIR}/module.modulemap MODULE_MAP)
            string(REPLACE "include/" "../include/" MODULE_MAP_INSTALL ${MODULE_MAP})

            file(WRITE ${CMAKE_BINARY_DIR}/module.modulemap.install "${MODULE_MAP_INSTALL}")
            install(FILES ${CMAKE_BINARY_DIR}/module.modulemap.install
                DESTINATION lib
                RENAME ${LIBRARY}.modulemap
            )

            install(CODE "execute_process(COMMAND touch ${CMAKE_INSTALL_PREFIX}/module.modulemap)")
        endif()

    endif()

    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/include/lib${LIBRARY}.Config.h.in)
        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/include/lib${LIBRARY}.Config.h DESTINATION ${CMAKE_INSTALL_PREFIX}/include/)
    endif()
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include DESTINATION ${CMAKE_INSTALL_PREFIX} 
            PATTERN "*.in" EXCLUDE PATTERN ".*" EXCLUDE)

    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/thislib.${LIBRARY}.sh DESTINATION ${CMAKE_INSTALL_PREFIX} 
            PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE OPTIONAL)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/thislib.${LIBRARY}.csh DESTINATION ${CMAKE_INSTALL_PREFIX} 
            PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE OPTIONAL)

    if(APPLE)
            install(FILES ${CMAKE_CURRENT_BINARY_DIR}/.rootlogon DESTINATION ${CMAKE_INSTALL_PREFIX} 
                    PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ OPTIONAL)
    else()
            install(FILES ${CMAKE_CURRENT_BINARY_DIR}/rootlogon.C DESTINATION ${CMAKE_INSTALL_PREFIX} 
                    PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ OPTIONAL)
    endif()

endmacro()
