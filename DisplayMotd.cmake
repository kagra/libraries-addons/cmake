# Turn into environment for motd display
set(ENV{PROJECT_NAME} ${PROJECT_NAME})
set(ENV{PROJECT_TITLE} ${PROJECT_TITLE})
set(ENV{PROJECT_VERSION} ${PROJECT_VERSION})
set(ENV{PROJECT_DESCRIPTION} ${PROJECT_DESCRIPTION})
set(ENV{PROJECT_HOMEPAGE_URL} ${PROJECT_HOMEPAGE_URL})

function(display_motd)

    # Define default locations to search for the MOTD file
    set(MOTD_LOCATIONS
        "${CMAKE_SOURCE_DIR}/motd"
        "${CMAKE_SOURCE_DIR}/share/motd"
        "${CMAKE_SOURCE_DIR}/docs/motd"
        "${CMAKE_SOURCE_DIR}/assets/motd"
        "${CMAKE_SOURCE_DIR}/cmake/motd"
    )

    # Variable to store the path of the found MOTD file
    set(MOTD_FOUND FALSE)

    # Determine whether to display MOTD
    set(MOTD_DISPLAY FALSE)
    if(DEFINED MOTD AND NOT "${MOTD}" STREQUAL "OFF" AND NOT "${MOTD}" STREQUAL "0")
        set(MOTD_DISPLAY TRUE)
    elseif(NOT DEFINED MOTD AND DEFINED ENV{MOTD} AND NOT "$ENV{MOTD}" STREQUAL "OFF" AND NOT "$ENV{MOTD}" STREQUAL "0")
        set(MOTD_DISPLAY TRUE)
    endif()

    # Proceed if MOTD_DISPLAY is TRUE
    if(MOTD_DISPLAY)

        # Check each location for the MOTD file
        foreach(MOTD_LOCATION ${MOTD_LOCATIONS})
            if(EXISTS "${MOTD_LOCATION}")
                set(MOTD "${MOTD_LOCATION}")
                set(MOTD_FOUND TRUE)
                break()
            endif()
        endforeach()

        # If the MOTD file is found, display its content
        if(MOTD_FOUND)
            file(READ "${MOTD}" MOTD_CONTENTS)
            if(NOT "${MOTD_CONTENTS}" STREQUAL "")
                execute_process(COMMAND sh "-c" "if [ ! -z \"$(which envsubst)\" ]; then echo \"$(envsubst < ${MOTD})\"; fi")
            endif()
        endif()
    endif()
endfunction()
