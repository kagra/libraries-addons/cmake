
# Function to load .env file
function(load_dotenv dotenv_file)
    if (EXISTS "${dotenv_file}")
        file(READ "${dotenv_file}" dotenv_content)

        # Split content into lines
        string(REPLACE "\n" ";" dotenv_lines "${dotenv_content}")

        # Process each line
        foreach (line IN LISTS dotenv_lines)

            # Skip comments and empty lines
            if (line MATCHES "^[ \t]*$" OR line MATCHES "^[ \t]*#.*$")
                continue()
            endif()

            # Match lines with "key=value"
            if (line MATCHES "^[ \t]*([^= \t]+)[ \t]*=[ \t]*(.*)$")
                string(REGEX REPLACE "^[ \t]*([^= \t]+)[ \t]*=[ \t]*(.*)$" "\\1" key "${line}")
                string(REGEX REPLACE "^[ \t]*([^= \t]+)[ \t]*=[ \t]*(.*)$" "\\2" value "${line}")

                # Set as environment variable
                set(ENV{${key}} "${value}")
            endif()
        endforeach()
    else()
        message(WARNING "The file ${dotenv_file} does not exist!")
    endif()
endfunction()

# Example usage
load_dotenv("${CMAKE_SOURCE_DIR}/.env")
