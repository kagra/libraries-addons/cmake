if(NOT TARGET distclean)

	add_custom_target(distclean
	    COMMAND ${CMAKE_COMMAND} -E remove_directory ${CMAKE_BINARY_DIR}/CMakeFiles
	    COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_BINARY_DIR}/Doxyfile
	    COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_BINARY_DIR}/Makefile
	    COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_BINARY_DIR}/CMakeCache.txt
	    COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_BINARY_DIR}/cmake_install.cmake
	    COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_BINARY_DIR}/CMakeDoxyfile.in
	    COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_BINARY_DIR}/CMakeDoxygenDefaults.cmake
	    COMMENT "Cleaning up temporary files"
	)

endif()
