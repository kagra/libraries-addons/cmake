set(CMAKE_MAINPROJECT OFF)
if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_SOURCE_DIR}")
	set(CMAKE_MAINPROJECT ON)
endif()

# Define a relative binary directory if contained within source
string(REPLACE "${CMAKE_SOURCE_DIR}/" "" CMAKE_BINARY_DIR_RELATIVE "${CMAKE_BINARY_DIR}")

function(setup_rpath_options)

    include(GNUInstallDirs) # Ensures cross-platform install paths

    # Build-time RPATH: Use for running from the build directory
    list(APPEND CMAKE_BUILD_RPATH "${CMAKE_BINARY_DIR}/lib")

    # Install-time RPATH: Set for installed binaries
    if(APPLE)
        set(CMAKE_MACOSX_RPATH TRUE)  # Ensure macOS RPATH handling is enabled
        list(APPEND CMAKE_INSTALL_RPATH "@rpath")
        list(APPEND CMAKE_INSTALL_RPATH "@loader_path/${CMAKE_INSTALL_LIBDIR}")
    else()
        list(APPEND CMAKE_INSTALL_RPATH "\$ORIGIN/${CMAKE_INSTALL_LIBDIR}")

        # Get system paths using ldconfig
        execute_process(
            COMMAND bash -c "ldconfig -v 2>/dev/null | awk -F: '/:/{print $1}'"
            OUTPUT_VARIABLE LDCONFIG_PATHS
            OUTPUT_STRIP_TRAILING_WHITESPACE
        )

        # Convert string output to a list
        string(REPLACE "\n" ";" SYSTEM_LIB_PATHS "${LDCONFIG_PATHS}")
        
        # Remove duplicates by filtering out CMAKE_INSTALL_LIBDIR
        foreach(PATH ${SYSTEM_LIB_PATHS})
            if (NOT "${PATH}" STREQUAL "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
                list(APPEND CMAKE_INSTALL_RPATH ${PATH})
            endif()
        endforeach()
    endif()

    # Also include the install prefix for runtime library lookup
    list(APPEND CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")

    # Ensure proper RPATH usage
    set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
    set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)
    set(CMAKE_SKIP_BUILD_RPATH TRUE)

endfunction()

if(NOT CMAKE_SKIP_RPATH)
    setup_rpath_options()
endif()

# Convert a string to a customizable snake_case format
function(SNAKE input output separator)
    # Default to underscore if no separator is provided
    if(NOT separator)
        set(separator "_")
    endif()

    # Convert camelCase or PascalCase to the specified snake_case
    string(REGEX REPLACE "([A-Z])" "${separator}\\1" snake_case "${input}")
    string(TOLOWER "${snake_case}" snake_case)

    # Remove leading separator(s)
    string(REGEX REPLACE "^[${separator}]+" "" snake_case "${snake_case}")

    # Remove trailing separator(s)
    string(REGEX REPLACE "[${separator}]+$" "" snake_case "${snake_case}")

    # Remove duplicate separator(s) within the string
    string(REGEX REPLACE "[${separator}]+${separator}" "${separator}" snake_case "${snake_case}")

    set(${output} "${snake_case}" PARENT_SCOPE)
endfunction()

# Convert a string to camelCase, casting into snake_case first
function(CAMEL input output)
    # Cast to snake_case first
    SNAKE("${input}" snake_case "-")

    # Convert snake_case to camelCase manually
    set(camel_case "")
    set(capitalize_next FALSE)  # First character should be lowercase

    # Iterate through each character of the snake_case string
    string(LENGTH "${snake_case}" length)
    foreach(i RANGE 0 ${length})
        string(SUBSTRING "${snake_case}" ${i} 1 char)
        if (char STREQUAL "-")
            set(capitalize_next TRUE)
        else()
            if (capitalize_next)
                string(TOUPPER "${char}" char)
                set(capitalize_next FALSE)
            else()
                string(TOLOWER "${char}" char)
            endif()
            set(camel_case "${camel_case}${char}")
        endif()
    endforeach()

    set(${output} "${camel_case}" PARENT_SCOPE)
endfunction()

# Convert a string to PascalCase by using CAMEL and capitalizing the first letter
function(PASCAL input output)
    # Convert to camelCase first
    CAMEL("${input}" camel_case)

    # Capitalize the first letter of camelCase result to get PascalCase
    string(SUBSTRING "${camel_case}" 0 1 first_char)
    string(TOUPPER "${first_char}" first_char)
    string(SUBSTRING "${camel_case}" 1 -1 rest_chars)
    set(pascal_case "${first_char}${rest_chars}")
    set(${output} "${pascal_case}" PARENT_SCOPE)
endfunction()

# Convert a string to uppercase
function(UPPER input output)
    string(TOUPPER "${input}" upper_case)
    set(${output} "${upper_case}" PARENT_SCOPE)
endfunction()

# Convert a string to lowercase
function(LOWER input output)
    string(TOLOWER "${input}" lower_case)
    set(${output} "${lower_case}" PARENT_SCOPE)
endfunction()

SNAKE (${PROJECT_NAME} PROJECT_NAME_SNAKE  "-")
CAMEL (${PROJECT_NAME} PROJECT_NAME_CAMEL )
PASCAL(${PROJECT_NAME} PROJECT_NAME_PASCAL)
LOWER (${PROJECT_NAME} PROJECT_NAME_LOWER)
UPPER (${PROJECT_NAME} PROJECT_NAME_UPPER)

# Includes all subdirectories found within the specified DIRECTORY_NAME
macro(INCLUDE_SUBDIRECTORIES DIRECTORY_NAME)
    file(GLOB DIRS "${DIRECTORY_NAME}/*")
    foreach(DIR ${DIRS})
        if(IS_DIRECTORY ${DIR})
            include_directories(${DIR})
        endif()
    endforeach()
endmacro()

function(FIND_FILES RESULT)

    # Parse the arguments passed to the function
    set(options RECURSE ABSOLUTE LIST_DIRECTORIES)
    set(multiValueArgs PATTERN EXCLUDE)
    cmake_parse_arguments(FF "${options}" "" "${multiValueArgs}" ${ARGN})

    # Initialize lists to store final file matches and patterns
    set(FILES_LIST)
    set(PATTERNS_REGEX)
    set(PATTERNS)
    set(IS_REGEX_PATTERN FALSE)
    set(PREVIOUS_PATTERN "")

    # Step 1: Separate non-regex and regex patterns
    foreach(PATTERN ${FF_PATTERN})
        if(PATTERN STREQUAL "REGEX")
            set(IS_REGEX_PATTERN TRUE)
        else()
            if(IS_REGEX_PATTERN)
                list(APPEND PATTERNS_REGEX ${PREVIOUS_PATTERN})
                set(IS_REGEX_PATTERN FALSE) # Reset regex state
            elseif(PREVIOUS_PATTERN)
                list(APPEND PATTERNS ${PREVIOUS_PATTERN})
            endif()
            set(PREVIOUS_PATTERN ${PATTERN})
        endif()
    endforeach()

    if(IS_REGEX_PATTERN)
        list(APPEND PATTERNS_REGEX ${PREVIOUS_PATTERN})
    elseif(PREVIOUS_PATTERN)
        list(APPEND PATTERNS ${PREVIOUS_PATTERN})
    endif()

    # Step 2a: Loop through each directory passed to the function
    foreach(DIR ${FF_UNPARSED_ARGUMENTS})
        
        if (NOT IS_ABSOLUTE ${DIR})
            set(DIR ${CMAKE_CURRENT_SOURCE_DIR}/${DIR})
        endif()

        if (NOT IS_DIRECTORY ${DIR})
            continue()
        endif()

        set(FILE_PATTERNS)
        foreach(PATTERN ${PATTERNS})
            list(APPEND FILE_PATTERNS "${DIR}/${PATTERN}")
        endforeach()
        if (NOT FILE_PATTERNS)
            list(APPEND FILE_PATTERNS "${DIR}/*")
        endif()

        if(FF_RECURSE)
            file(GLOB_RECURSE NON_REGEX_FILES LIST_DIRECTORIES ${FF_LIST_DIRECTORIES} ${FILE_PATTERNS})
        else()
            file(GLOB NON_REGEX_FILES LIST_DIRECTORIES ${FF_LIST_DIRECTORIES} ${FILE_PATTERNS})
        endif()

        # Step 2b: Gather all files using regex patterns
        set(ALL_FILES)
        if(FF_RECURSE)
            file(GLOB_RECURSE ALL_FILES LIST_DIRECTORIES ${FF_LIST_DIRECTORIES} "${DIR}/*")
        else()
            file(GLOB ALL_FILES LIST_DIRECTORIES ${FF_LIST_DIRECTORIES} "${DIR}/*")
        endif()

        set(REGEX_FILES)
        foreach(FILE ${ALL_FILES})
            
            if (FF_LIST_DIRECTORIES AND IS_DIRECTORY ${FILE})
                list(APPEND NON_REGEX_FILES ${FILE})
            else()
                file(RELATIVE_PATH FILE_RELATIVE "${DIR}/" "${FILE}")
                foreach(PATTERN ${PATTERNS_REGEX})   
                    if(FILE_RELATIVE MATCHES ${PATTERN} )
                        list(APPEND REGEX_FILES ${FILE})
                        break()
                    endif()
                endforeach()
            endif()
        endforeach()

        # Step 3: Combine non-regex and regex matches and apply exclusion patterns
        list(REMOVE_DUPLICATES NON_REGEX_FILES)
        list(REMOVE_DUPLICATES REGEX_FILES)
        list(APPEND _FILES_LIST ${NON_REGEX_FILES} ${REGEX_FILES})

        foreach(FILE IN LISTS _FILES_LIST)

            if(FF_ABSOLUTE)
                if (NOT IS_ABSOLUTE ${FILE})
                    get_filename_component(ABS_PATH ${CMAKE_CURRENT_SOURCE_DIR}/${FILE} ABSOLUTE)
                    set(ITEM ${ABS_PATH})
                else()
                    set(ITEM ${FILE})
                endif()
            else()
                if (NOT IS_ABSOLUTE ${FILE})
                set(ITEM ${FILE})
                else()
                    file(RELATIVE_PATH FILE "${CMAKE_CURRENT_SOURCE_DIR}/" "${FILE}")
                    set(ITEM ${FILE})
                endif()
            endif()
    
            list(APPEND FILES_LIST ${ITEM}) # Insert each item at the beginning
            
        endforeach()

    endforeach()

    # Step 4: Apply exclusion patterns
    foreach(EXCLUDE ${FF_EXCLUDE})
        if(EXCLUDE STREQUAL "REGEX")
            set(IS_REGEX_EXCLUDE TRUE)
        else()
            if(IS_REGEX_EXCLUDE)
                list(APPEND EXCLUDES_REGEX ${PREVIOUS_EXCLUDE})
                set(IS_REGEX_EXCLUDE FALSE) # Reset regex state
            elseif(PREVIOUS_EXCLUDE)
                list(APPEND EXCLUDES ${PREVIOUS_EXCLUDE})
            endif()
            set(PREVIOUS_EXCLUDE ${EXCLUDE})
        endif()
    endforeach()

    if(IS_REGEX_EXCLUDE)
        list(APPEND EXCLUDES_REGEX ${PREVIOUS_EXCLUDE})
    elseif(PREVIOUS_EXCLUDE)
        list(APPEND EXCLUDES ${PREVIOUS_EXCLUDE})
    endif()

    set(FILTERED_FILES)
    foreach(FILE ${FILES_LIST})

        if (IS_ABSOLUTE ${FILE})
            file(RELATIVE_PATH FILE_RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/" "${FILE}")
        else()
            set(FILE_RELATIVE ${FILE})
        endif()

        set(MATCH_EXCLUDE FALSE)

        if(FF_EXCLUDE)

            foreach(EXCLUDE_PATTERN ${EXCLUDES})

                # Clean up the exclusion pattern (remove leading/trailing slashes)
                if(NOT FF_ABSOLUTE)
                    string(REGEX REPLACE "^/" "" EXCLUDE_PATTERN "${EXCLUDE_PATTERN}")
                    string(REGEX REPLACE "/$" "" EXCLUDE_PATTERN "${EXCLUDE_PATTERN}")
                endif()

                # Check if the file path contains the exclusion pattern (simple check)
                string(FIND "${FILE_RELATIVE}" "${EXCLUDE_PATTERN}" MATCH_POS)
                if(MATCH_POS GREATER_EQUAL 0)
                    set(MATCH_EXCLUDE TRUE)
                endif()

                # Compute the list of excluded files
                if(NOT MATCH_EXCLUDE)

                    foreach(DIR ${FF_UNPARSED_ARGUMENTS})

                        if (NOT IS_ABSOLUTE ${DIR})
                            set(DIR ${CMAKE_CURRENT_SOURCE_DIR}/${DIR})
                        endif()

                        if (NOT IS_DIRECTORY ${DIR})
                            continue()
                        endif()

                        set(EXCLUDED_FILES)
                        if(FF_RECURSE)
                            file(GLOB_RECURSE EXCLUDED_FILES LIST_DIRECTORIES ${FF_LIST_DIRECTORIES} "${DIR}/${EXCLUDE_PATTERN}")
                        else()
                            file(GLOB EXCLUDED_FILES LIST_DIRECTORIES ${FF_LIST_DIRECTORIES} "${DIR}/${EXCLUDE_PATTERN}")
                        endif()

                        foreach(EXCLUDED_FILE IN LISTS EXCLUDED_FILES)
                            if(NOT FF_ABSOLUTE)
                                file(RELATIVE_PATH EXCLUDED_FILE_RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "${EXCLUDED_FILE}")
                                if (EXCLUDED_FILE_RELATIVE STREQUAL FILE_RELATIVE)
                                    set(MATCH_EXCLUDE TRUE)
                                    break()
                                endif()
                            else()
                                if (EXCLUDED_FILE STREQUAL FILE)
                                    set(MATCH_EXCLUDE TRUE)
                                    break()
                                endif()
                            endif()
                        endforeach()

                        if(MATCH_EXCLUDE)
                            break()
                        endif()
                    endforeach()

                    if(MATCH_EXCLUDE)
                        break()
                    endif()

                endif()

            endforeach()

            if(NOT MATCH_EXCLUDE)

                foreach(EXCLUDE_PATTERN ${EXCLUDES_REGEX})
                    string(REGEX REPLACE "^/" "" EXCLUDE_PATTERN "${EXCLUDE_PATTERN}")
                    string(REGEX REPLACE "/$" "" EXCLUDE_PATTERN "${EXCLUDE_PATTERN}")

                    if(FF_ABSOLUTE)
                        if(FILE MATCHES ${EXCLUDE_PATTERN})
                            set(MATCH_EXCLUDE TRUE)
                            break()
                        endif()
                    else()
                        if(FILE_RELATIVE MATCHES ${EXCLUDE_PATTERN})
                            set(MATCH_EXCLUDE TRUE)
                            break()
                        endif()
                    endif()
                endforeach()
            endif()
        endif()

        if(NOT MATCH_EXCLUDE)
            list(APPEND FILTERED_FILES ${FILE})
        endif()

    endforeach()

    list(REMOVE_DUPLICATES FILTERED_FILES)
    list(SORT FILTERED_FILES)

    set(${RESULT} ${FILTERED_FILES} PARENT_SCOPE)

endfunction()

# FILE_SOURCES macro using FIND_FILES
macro(FILE_SOURCES RESULT_VAR)
    FIND_FILES(${RESULT_VAR} ${ARGN} RECURSE
        PATTERN "\\.[Cc]$" REGEX 
        PATTERN "*.cc" "*.cxx" "*.cpp"
        EXCLUDE "/__pycache__/" "*.txt" "*.o" ".*"
    )
endmacro()

# FILE_HEADERS macro using FIND_FILES
macro(FILE_HEADERS RESULT_VAR)
    FIND_FILES(${RESULT_VAR} ${ARGN}
        PATTERN "*.hh" "*.h" "*.hxx" "*.pyh" "*.pxi"
        EXCLUDE "/__pycache__/" "*.txt" ".*"
    )
endmacro()

# FILE_PYTHON macro using FIND_FILES
macro(FILE_PYTHON RESULT_VAR)
    FIND_FILES(${RESULT_VAR} ${ARGN} RECURSE
        PATTERN "*.py" "*.pyx" "*.pyh" "*.pxd" "*.pxi" "*.pxd.in" "*.pxi.in"
        PATTERN "/[^\\.]+$" REGEX
        EXCLUDE "/__pycache__/" "*.txt" "*.pyc" ".*"
    )
endmacro()


# Macro to sort files by directory first
macro(SORT_BY_DIRECTORY files_list out_sorted_list)
    set(sorted_list "")
    foreach(file ${${files_list}})
        if(IS_DIRECTORY ${file})
            list(APPEND sorted_list "D:${file}")
        else()
            list(APPEND sorted_list "F:${file}")
        endif()
    endforeach()

    list(SORT sorted_list)
    foreach(item ${sorted_list})
        string(REGEX REPLACE "^(D|F):" "" item ${item})
        list(APPEND ${out_sorted_list} ${item})
    endforeach()
endmacro()

# Function: dump_cmake_variables
# Prints CMake variables, optionally filtered by a regular expression
function(DUMP_CMAKE_VARIABLES)
    get_cmake_property(_variableNames VARIABLES)
    list(SORT _variableNames)
    foreach(_variableName ${_variableNames})
        if (ARGV0)
            unset(MATCHED)
            string(REGEX MATCH ${ARGV0} MATCHED ${_variableName})
            if (NOT MATCHED)
                continue()
            endif()
        endif()
        message(STATUS "${_variableName}=${${_variableName}}")
    endforeach()
endfunction()

function(message_title msg)

    string(LENGTH "${msg}" name_length)
    math(EXPR dash_count "${name_length} + 14") # Considering additional characters and spaces

    string(REPEAT "-" ${dash_count} dashes)
    message("")
    message("-- ${dashes} --")
    message("-- ----- ${msg}   ----- --")
    message("-- ${dashes} --")
endfunction()

function(IMPLODE outStr delimiter inputList)
    set(result "")
    list(LENGTH ${inputList} count)
    math(EXPR last_index "${count} - 1")

    foreach(i RANGE ${last_index})
        list(GET ${inputList} ${i} elem)
        if(result)
            set(result "${result}${delimiter}${elem}")
        else()
            set(result "${elem}")
        endif()
    endforeach()

    set(${outStr} "${result}" PARENT_SCOPE)
endfunction()

# Find the common root path among all files in all input lists
function(GET_COMMON_PATH RESULT_PATH)

    cmake_parse_arguments(CP "RELATIVE" "" "" ${ARGN})
    set(INPUT_LISTS ${DP_UNPARSED_ARGUMENTS})
    
    set(COMMON_PATH)
    foreach(INPUT_LIST ${INPUT_LISTS})

        foreach(FILE_PATH ${${INPUT_LIST}})

            get_filename_component(PATH_PARENT "${FILE_PATH}" DIRECTORY)
            if (NOT DEFINED COMMON_PATH)
                set(COMMON_PATH "${PATH_PARENT}")
            else()
                while (NOT ${PATH_PARENT} MATCHES "^${COMMON_PATH}")
                    get_filename_component(COMMON_PATH "${COMMON_PATH}" DIRECTORY)
                    if(COMMON_PATH STREQUAL "")
                        break()
                    endif()
                endwhile()
            endif()

        endforeach()
    endforeach()

    if (CP_RELATIVE AND NOT COMMON_PATH)
        set(COMMON_PATH "./")
    elseif (NOT CP_RELATIVE AND NOT COMMON_PATH)
        set(COMMON_PATH "${CMAKE_CURRENT_SOURCE_DIR}")
    elseif (NOT CP_RELATIVE AND NOT IS_ABSOLUTE ${COMMON_PATH})
        set(COMMON_PATH "${CMAKE_CURRENT_SOURCE_DIR}/${COMMON_PATH}")
    endif()

    set(${RESULT_PATH} ${COMMON_PATH} PARENT_SCOPE)

endfunction()

# Macro: dump_architecture
# Displays the project's architecture based on a list of files
function(__DUMP_ARCHITECTURE)

    # Collect all input lists into a single list
    set(INPUT_LISTS ${ARGV})

    GET_COMMON_PATH(COMMON_ROOT ${ARGV})
    foreach(INPUT_LIST ${INPUT_LISTS})

        list(LENGTH ${INPUT_LIST} FILE_PATH_LENGTH)
        if(FILE_PATH_LENGTH GREATER 0)
                message("   |")
        endif()

        set(FILE_PATHS "${${INPUT_LIST}}")
        foreach(FILE_PATH ${FILE_PATHS})

        # Replace backslashes with forward slashes for Windows paths
        string(REPLACE "\\" "/" FILE_PATH "${FILE_PATH}")
        # Remove the common root path from each file path
        string(REPLACE "${COMMON_ROOT}/" "" SHORTENED_PATH ${FILE_PATH})
        # Split the shortened file path into individual directories
        string(REPLACE "/" ";" SHORTENED_PATH_LIST ${SHORTENED_PATH})
        
        # Initialize a variable to store the formatted path
        set(FORMATTED_PATH "")
        if (NOT IS_ABSOLUTE ${FILE_PATH})
            set(FILE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/${FILE_PATH}")
        endif()
        
        # Iterate through each directory level to create the tree-like structure
            foreach(PATH_ELEMENT ${SHORTENED_PATH_LIST})
                # Exclude empty path elements
                if(NOT "${PATH_ELEMENT}" STREQUAL "")
                    # Append the shortened directory level to the formatted path
                    set(FORMATTED_PATH "${FORMATTED_PATH}/${PATH_ELEMENT}")
                endif()
            endforeach()
            
            # Display the formatted tree structure with shortened paths
            if(IS_DIRECTORY "${FILE_PATH}")
                message("   |-- .${FORMATTED_PATH}/[..]")
            else()
                message("   |-- .${FORMATTED_PATH}")
            endif()

        endforeach()
    endforeach()

endfunction()

# Macro: dump_project
# Gathers source, header, test, and example files, then displays the entire project architecture
function(dump_project)

    # Gather C/C++ source and header files
    FIND_FILES(SOURCES "src" EXCLUDE "/__pycache__/" "*.txt" ".*")

    FILE_HEADERS(HEADERS "include" LIST_DIRECTORIES)

    # Gather Python files
    FILE_PYTHON(PYTHON "python" "modules" RECURSE LIST_DIRECTORIES)

    # Gather test files
    FIND_FILES(TESTS "tests" LIST_DIRECTORIES EXCLUDE "/__pycache__/" "*.txt" ".*")

    # Gather example files
    FIND_FILES(EXAMPLES "examples" LIST_DIRECTORIES EXCLUDE "/__pycache__/" "*.txt" ".*")

    # Collect all input lists into a single list
    set(INPUT_LISTS SOURCES HEADERS PYTHON TESTS EXAMPLES)
    GET_COMMON_PATH(COMMON_ROOT ${INPUT_LISTS} ABSOLUTE)

    set(IS_EMPTY ON)
    foreach(INPUT_LIST ${INPUT_LISTS})
        list(LENGTH ${INPUT_LIST} LIST_LENGTH)
        if(LIST_LENGTH GREATER 0)
            set(IS_EMPTY OFF)
        endif()
    endforeach()

    # Display the entire project architecture
    if(NOT IS_EMPTY)
        message(STATUS "Project architecture: ${COMMON_ROOT}")
        __DUMP_ARCHITECTURE(${INPUT_LISTS})
    else()
        message(STATUS "Project architecture is empty")
    endif()

endfunction()

function(DUMP_ARCHITECTURE)

    # Parse the arguments passed to the function
    cmake_parse_arguments(DA "" "RELATIVE_PATH" "" ${ARGN})
    set(INPUT_LISTS ${DA_UNPARSED_ARGUMENTS})
    GET_COMMON_PATH(COMMON_ROOT ${ARGV})

    set(IS_EMPTY ON)
    foreach(INPUT_LIST ${INPUT_LISTS})
        list(LENGTH ${INPUT_LIST} LIST_LENGTH)
        if(LIST_LENGTH GREATER 0)
            set(IS_EMPTY OFF)
        endif()
    endforeach()
    
    # Dump architecture
    if(NOT IS_EMPTY)

        if(DA_RELATIVE_PATH)
            if(NOT IS_ABSOLUTE ${COMMON_ROOT})
                set(COMMON_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/${COMMON_ROOT}")
            endif()
            file(RELATIVE_PATH COMMON_ROOT "${DA_RELATIVE_PATH}" "${COMMON_ROOT}")
            message(STATUS "Architecture path: ./${COMMON_ROOT}")
        else()
            message(STATUS "Architecture path: ${COMMON_ROOT}")
        endif()
        
        __DUMP_ARCHITECTURE(${INPUT_LISTS})

    else()
        message(STATUS "Architecture is empty")
    endif()

endfunction()

# Function to check and display library information if verbose mode is enabled
function(dump_target_dependencies)

    get_property(targets DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY BUILDSYSTEM_TARGETS)

    foreach(target ${targets})
        
        get_target_property(target_libraries ${target} INTERFACE_LINK_LIBRARIES)
        if(target_libraries)
            message(STATUS "Target `${target}` has the following public dependencies:")
            foreach(library ${target_libraries})
            message(STATUS "\t${library}")
            endforeach()
        else()
            message(STATUS "Target `${target}` has no public libraries linked.")
        endif()
        
    endforeach()
endfunction()

function(check_target_dependencies)

    get_property(targets DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY BUILDSYSTEM_TARGETS)

    # Loop over all the available targets
    foreach(target ${targets})

        get_target_property(target_type ${target} TYPE)
        if(target_type STREQUAL "INTERFACE_LIBRARY" OR target_type STREQUAL "UTILITY" OR target_type STREQUAL "OBJECT_LIBRARY")
            message(STATUS "Skipping post-build dependency check for interface target `${target}`")
            continue()
        endif()

        message(STATUS "Target linkage will be displayed at post-build stage for ${target_type} `${target}`")

        if(APPLE)

            add_custom_command(
                TARGET ${target} POST_BUILD
                COMMAND otool -L $<TARGET_FILE:${target}>  # On macOS
                COMMENT "Checking post-build dependencies for ${target}"
            )

        else()

            add_custom_command(
                TARGET ${target} POST_BUILD
                COMMAND ldd $<TARGET_FILE:${target}>  # On Linux
                COMMENT "Checking post-build dependencies for ${target}"
            )
        endif()
    endforeach()
    
endfunction()