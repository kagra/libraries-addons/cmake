
# Check supported C++ standards
include(CheckCXXCompilerFlag)

# Check for different C++ standards
check_cxx_compiler_flag("-std=c++98" COMPILER_SUPPORTS_CXX98)
check_cxx_compiler_flag("-std=c++03" COMPILER_SUPPORTS_CXX03)
check_cxx_compiler_flag("-std=c++11" COMPILER_SUPPORTS_CXX11)
check_cxx_compiler_flag("-std=c++14" COMPILER_SUPPORTS_CXX14)
check_cxx_compiler_flag("-std=c++17" COMPILER_SUPPORTS_CXX17)
check_cxx_compiler_flag("-std=c++20" COMPILER_SUPPORTS_CXX20)
check_cxx_compiler_flag("-std=c++23" COMPILER_SUPPORTS_CXX23)

if(COMPILER_SUPPORTS_CXX23)
    set(SUPPORTED_CXX_STANDARDS 23 20 17 14 11 03 98)
elseif(COMPILER_SUPPORTS_CXX20)
    set(SUPPORTED_CXX_STANDARDS 20 17 14 11 03 98)
elseif(COMPILER_SUPPORTS_CXX17)
    set(SUPPORTED_CXX_STANDARDS 17 14 11 03 98)
elseif(COMPILER_SUPPORTS_CXX14)
    set(SUPPORTED_CXX_STANDARDS 14 11 03 98)
elseif(COMPILER_SUPPORTS_CXX11)
    set(SUPPORTED_CXX_STANDARDS 11 03 98)
elseif(COMPILER_SUPPORTS_CXX03)
    set(SUPPORTED_CXX_STANDARDS 03 98)
elseif(COMPILER_SUPPORTS_CXX98)
    set(SUPPORTED_CXX_STANDARDS 98)
else()
    message(FATAL_ERROR "No supported C++ standard found.")
endif()

# Allow manual override of C++ standard
if(NOT DEFINED CMAKE_CXX_STANDARD)
    # Use the highest supported C++ standard as default
    set(HIGHEST_SUPPORTED_CXX_STANDARD ${SUPPORTED_CXX_STANDARDS})
    list(GET HIGHEST_SUPPORTED_CXX_STANDARD 0 CMAKE_CXX_STANDARD)
endif()

# Print the selected C++ standard
if(NOT CMAKE_CXX_HASH STREQUAL "${CMAKE_CXX_COMPILER}${CMAKE_CXX_STANDARD}")
    
    # Update the cached hash value
    set(CMAKE_CXX_HASH "${CMAKE_CXX_COMPILER}${CMAKE_CXX_STANDARD}" CACHE INTERNAL "Hash of CMAKE_CXX_COMPILER and CMAKE_CXX_STANDARD")

    # Only print the message when CMAKE_CXX_COMPILER or CMAKE_CXX_STANDARD has changed
    if(CMAKE_CXX_COMPILER)
        message(STATUS "Found C++${CMAKE_CXX_STANDARD} standard compiler: ${CMAKE_CXX_COMPILER}")
    else()
        message(FATAL_ERROR "No C++ standard compiler found.")
    endif()
endif()