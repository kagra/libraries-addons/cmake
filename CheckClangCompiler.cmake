# Check if CMake is compiled with Clang
include(CheckCXXCompilerFlag)

# Specify a Clang-specific flag to check (for example, -fcolor-diagnostics)
check_cxx_compiler_flag("-fcolor-diagnostics" COMPILER_SUPPORTS_CLANG)

if(NOT COMPILER_SUPPORTS_CLANG)
    message(WARNING "It is recommended to compile this project with Clang.")
endif()