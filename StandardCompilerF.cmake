
# Enable Fortran language
enable_language(Fortran)

# Check supported Fortran standards
include(CheckFortranCompilerFlag)

# Check for different Fortran standards
check_fortran_compiler_flag("-std=f95" COMPILER_SUPPORTS_F95)
check_fortran_compiler_flag("-std=f2003" COMPILER_SUPPORTS_F2003)
check_fortran_compiler_flag("-std=f2008" COMPILER_SUPPORTS_F2008)
check_fortran_compiler_flag("-std=f2018" COMPILER_SUPPORTS_F2018)

if(COMPILER_SUPPORTS_F2018)
    set(SUPPORTED_FORTRAN_STANDARDS 2018 2008 2003 95)
elseif(COMPILER_SUPPORTS_F2008)
    set(SUPPORTED_FORTRAN_STANDARDS 2008 2003 95)
elseif(COMPILER_SUPPORTS_F2003)
    set(SUPPORTED_FORTRAN_STANDARDS 2003 95)
elseif(COMPILER_SUPPORTS_F95)
    set(SUPPORTED_FORTRAN_STANDARDS 95)
else()
    message(FATAL_ERROR "No supported Fortran standard found.")
endif()

# Allow manual override of C standard
if(NOT DEFINED CMAKE_F_STANDARD)
    # Use the highest supported C standard as default
    set(HIGHEST_SUPPORTED_F_STANDARD ${SUPPORTED_F_STANDARDS})
    list(GET HIGHEST_SUPPORTED_F_STANDARD 0 CMAKE_F_STANDARD)
endif()

# Print the selected C standard
if(NOT CMAKE_F_HASH STREQUAL "${CMAKE_F_COMPILER}${CMAKE_F_STANDARD}")
    
    # Update the cached hash value
    set(CMAKE_F_HASH "${CMAKE_F_COMPILER}${CMAKE_F_STANDARD}" CACHE INTERNAL "Hash of CMAKE_F_COMPILER and CMAKE_F_STANDARD")

    # Print the selected Fortran standard
    if(CMAKE_Fortran_COMPILER)
        message(STATUS "Found Fortran F${CMAKE_Fortran_STANDARD} standard compiler: ${CMAKE_Fortran_COMPILER}")
    else()
        message(FATAL_ERROR "No Fortran standard compiler found.")
    endif()
endif()